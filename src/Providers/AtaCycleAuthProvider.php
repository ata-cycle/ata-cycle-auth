<?php

namespace Ata\Cycle\Auth\Providers;

use Cycle\ORM\ORM;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class AtaCycleAuthProvider implements UserProvider
{
    /**
     * @var \Cycle\ORM\RepositoryInterface
     */
    private $repository;

    /**
     * @var string
     */
    private $source;

    /**
     * @var HasherContract
     */
    private $hasher;

    public function __construct(ORM $orm, string $userClass, HasherContract $hasher)
    {
        $this->repository = $orm->getRepository($userClass);
        $this->source = $orm->getSource($userClass);

        $this->hasher = $hasher;
    }

    /**
     * @inheritDoc
     */
    public function retrieveById($identifier)
    {
        return $this->repository->findByPK($identifier);
    }

    /**
     * @inheritDoc
     */
    public function retrieveByToken($identifier, $token)
    {
        $user = $this->retrieveById($identifier);

        return $user && $user->getRememberToken() && hash_equals($user->getRememberToken(), $token)
            ? $user : null;
    }

    /**
     * @inheritDoc
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $this->source->getDatabase()->table($this->source->getTable())
            ->update([$user->getRememberTokenName() => $token],
                [$user->getAuthIdentifierName(), $user->getAuthIdentifier()]
            )->run();
    }

    /**
     * @inheritDoc
     */
    public function retrieveByCredentials(array $credentials)
    {
        unset($credentials['password']);

        if (empty($credentials)) {
            return null;
        }

        return $this->repository->findOne($credentials);
    }

    /**
     * Get the generic user.
     *
     * @param  mixed  $user
     * @return \Illuminate\Auth\GenericUser|null
     */
    protected function getGenericUser($user)
    {
        if (! is_null($user)) {
            return new GenericUser((array) $user);
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $this->hasher->check(
            $credentials['password'], $user->getAuthPassword()
        );
    }
}
