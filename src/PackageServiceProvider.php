<?php


namespace Ata\Cycle\Auth;

use Ata\Cycle\Auth\Providers\AtaCycleAuthProvider;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    public function register(){
        Auth::provider('ata-cycle', function ($app, array $config) {
            return new AtaCycleAuthProvider(resolve('cycle-db'), config('cycle.auth.user'), $app->make(Hasher::class));
        });
    }

    public function boot(){
        $this->mergeConfigFrom(
            __DIR__ . '/../config/cycle.php', 'cycle.auth'
        );
    }
}
