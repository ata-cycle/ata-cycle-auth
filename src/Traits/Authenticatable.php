<?php


namespace Ata\Cycle\Auth\Traits;

use Cycle\Annotated\Annotation\Column;

/**
 * Realization of Illuminate/Auth/Interfaces/Authenticatable
 */
trait Authenticatable
{
    /** @Column(type="string") */
    private $password;

    /** @Column(type="string", nullable=true) */
    public $remember_token;

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'id';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->id;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string|null
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function setPasswordAttribute($value)
    {
        $value = bcrypt($value);
        $this->password = $value;
    }
}
