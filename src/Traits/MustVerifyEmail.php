<?php

namespace Ata\Cycle\Auth\Traits;

use Carbon\CarbonImmutable;
use Cycle\Annotated\Annotation\Column;
use Illuminate\Auth\Notifications\VerifyEmail;
use Ata\Cycle\ORM\Typecasts\Carbon;

trait MustVerifyEmail
{
    /** @Column(type="timestamp", nullable=true, typecast=Carbon::class) */
    public $email_verified_at;

    /**
     * Determine if the user has verified their email address.
     *
     * @return bool
     */
    public function hasVerifiedEmail()
    {
        return $this->email_verified_at !== null;
    }

    /**
     * Mark the given user's email as verified.
     *
     * @throws \Exception
     */
    public function markEmailAsVerified()
    {
        $this->update(['email_verified_at'=>new CarbonImmutable()]);
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * Get the email address that should be used for verification.
     *
     * @return string
     */
    public function getEmailForVerification()
    {
        return $this->email;
    }
}
